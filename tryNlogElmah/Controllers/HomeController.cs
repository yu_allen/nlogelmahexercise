﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using System.Reflection;
using NLog;

namespace tryNlogElmah.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Logger _logger = LogManager.GetCurrentClassLogger();
            _logger.Fatal("TEST-Fatal");
            _logger.Error("TEST-Error");

            //string a = null;
            //string b = a.ToString();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}